import boto3
import logging

from voluptuous import Schema, Required, Optional, Invalid
from validate_email import validate_email

class InvalidContent(Exception):
    pass


class InternalError(Exception):
    pass


logger = logging.getLogger()

def validate_email_list(email_list):
    is_list = isinstance(email_list, list)
    has_invalid_email = any(not validate_email(str(email)) for email in set(email_list))
    if not is_list or has_invalid_email:
        raise Invalid('email validation failed; is_list: %s; has_invalid_email: %s' % (str(is_list), str(has_invalid_email)))
    return email_list

def validate_source_email(email):
    if not validate_email(email):
        raise Invalid('email validation failed; has_invalid_email: %s' % (str(is_list), str(has_invalid_email)))
    return email

def validate_simple_content(content):
    schema = Schema({
        Optional('Source', default='noreply@aiss.hu'): validate_source_email,
        Required('Destination'): {
            Required('ToAddresses'): validate_email_list,
            Optional('CcAddresses'): validate_email_list,
            Optional('BccAddresses'): validate_email_list
        },
        Required('Message'): {
            Required('Subject'): {
                Required('Data'): unicode,
                Optional('Charset'): unicode
            },
            Optional('Body'): {
                'Text': {
                    Required('Data'): unicode,
                    Optional('Charset'): unicode
                },
                'Html': {
                    Required('Data'): unicode,
                    Optional('Charset'): unicode
                }
            }
        }
    })
    return schema(content)

def validate_raw_content(content):
    schema = Schema({
        Optional('Source', default='noreply@aiss.hu'): validate_source_email,
        Required('Destination'): validate_email_list,
        Required('Message'): {
            Required('Data'): unicode
        }
    })
    return schema(content)

def get_validator(query):
    if query['raw'] == "true":
        return validate_raw_content
    return validate_simple_content

def handler(event, context):
    try:
        response = None
        body = event['body']
        query = event['query']
        validator = get_validator(query)
        body = validator(body) # TODO: beautify
        logger.info('content is valid')
        client = boto3.client('ses')
        # TODO: this part is ugly, refactor needed
        if query['raw'] == "true":
            response = client.send_raw_email(
                Source = body['Source'],
                Destinations = body['Destination'],
                RawMessage = body['Message']
            )
        else:
            response = client.send_email(
                Source = body['Source'],
                Destination = body['Destination'],
                Message = body['Message']
            )
        logger.info('message sent; id: %s' % (response))
    except Invalid as e:
        reason = "InvalidContent: " + str(e)
        logger.error("invalid content; reason: %s" % (reason))
        raise InvalidContent(reason)
    except Exception as e:
        # TODO: Try to store for sending later
        reason = 'InternalError: unable to send message because of internal error'
        internal_reason = str(e)
        logger.error("internal error; reason: %s" % (reason))
        raise InternalError(reason)
    return response
